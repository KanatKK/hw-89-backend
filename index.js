const express = require("express");
const cors = require("cors");
const mongoose = require("mongoose");
const artists = require("./app/artist");
const albums = require("./app/album");
const track = require("./app/track");
const user = require("./app/users");
const trackHistory = require("./app/trackHistory");
const config = require("./config");
const app = express();
const port = process.env.NODE_ENV === "test" ? 8010 : 8000;

app.use(cors());
app.use(express.json());
app.use(express.static("public"));

const run = async () => {
  await mongoose.connect(config.db.url + "/" + config.db.name, {useNewUrlParser: true});

  app.use("/artists", artists);
  app.use("/albums", albums);
  app.use("/tracks", track);
  app.use("/users", user);
  app.use("/track_history", trackHistory);

  console.log("Connected to mongo DB");

  app.listen(port, () => {
    console.log(`Server started at http://localhost:${port}`);
  });
};

run().catch(console.log);


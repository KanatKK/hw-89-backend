const mongoose = require("mongoose");
const config = require("./config");
const Album = require("./models/Album");
const Artist = require("./models/Artist");
const Track = require("./models/Track");
const User = require("./models/User");
const TrackHistory = require("./models/TrackHistory");
const {nanoid} = require("nanoid");

mongoose.connect(config.db.url + "/" + config.db.name, {useNewUrlParser: true});

const db = mongoose.connection;

db.once("open", async () => {
   try  {
       await db.dropCollection('albums');
       await db.dropCollection('artists');
       await db.dropCollection('tracks');
       await db.dropCollection('trackhistories');
       await db.dropCollection('users');
   } catch (e) {
       console.log("Collection were not presented, skipping drop...");
   }

   const [XXXTENTACION, LilPeep] = await Artist.create({
       name: "XXXTENTACION",
       description: "Some info about X",
       image: "pab7iIpE6qPUHv5WpnqIc.jpg",
       published: true
   }, {
       name: "Lil Peep",
       description: "Some info about Lil Peep",
       image: "KkHH8PfvRrPZfC6lGZQFq.jpg",
       published: true
   });

   const [seventeen, crybaby, questionMark] = await Album.create({
       name: "17",
       artist: XXXTENTACION._id,
       year: 2017,
       image: "7VmA7ljP9yFvpwGCUZ1CO.jpg",
       published: true
   }, {
       name: "Crybaby",
       artist: LilPeep._id,
       year: 2016,
       image: "HSGMJ5k5gEz98sWksmSc8.jpg",
       published: true
   }, {
       name: "?",
       artist: XXXTENTACION._id,
       year: 2018,
       image: "Rl5q70aC6So_72szP2Me_.jpg",
       published: true
   });

    await Track.create({
        name: "The Explanation",
        album: seventeen._id,
        duration: "0:50",
        number: 1,
        published: true
    },{
        name: "Jocelyn Flores",
        album: seventeen._id,
        duration: "1:59",
        number: 2,
        published: true
    },{
        name: "Depression & Obsession",
        album: seventeen._id,
        duration: "2:24",
        number: 3,
        published: true
    },{
        name: "Everybody Dies in Their Nightmares",
        album: seventeen._id,
        duration: "1:35",
        number: 4,
        published: true
    },{
        name: "Revenge",
        album: seventeen._id,
        duration: "2:00",
        number: 5,
        published: true
    }, {
        name: "Crybaby",
        album: crybaby._id,
        duration: "4:08",
        number: 1,
        published: true
    }, {
        name: "Lil Jeep",
        album: crybaby._id,
        duration: "3:22",
        number: 2,
        published: true
    }, {
        name: "Yesterday",
        album: crybaby._id,
        duration: "1:51",
        number: 3,
        published: true
    }, {
        name: "Absolute in Doubt",
        album: crybaby._id,
        duration: "3:23",
        number: 4,
        published: true
    }, {
        name: "Ghost Girl",
        album: crybaby._id,
        duration: "2:53",
        number: 5,
        published: true
    }, {
        name: "Introduction (Instructions)",
        album: questionMark._id,
        duration: "1:57",
        number: 1,
        published: true
    },{
        name: "Alone, Part 3",
        album: questionMark._id,
        duration: "1:49",
        number: 2,
        published: true
    },{
        name: "Moonlight",
        album: questionMark._id,
        duration: "2:15",
        number: 3,
        published: true
    }, {
        name: "Sad!",
        album: questionMark._id,
        duration: "2:46",
        number: 4,
        published: true
    }, {
        name: "The Remedy for a Broken Heart (Why Am I So in Love)",
        album: questionMark._id,
        duration: "2:40",
        number: 5,
        published: true
    },);

    const [user, admin] = await User.create({
        username: "user",
        password: "1234",
        role: "user",
        displayName: "Rick",
        imageLink: "https://avatarfiles.alphacoders.com/128/128984.png",
        token: nanoid(),
    }, {
        username: "admin",
        password: "4321",
        role: "admin",
        displayName: "Logan",
        imageLink: "https://s3.ap-south-1.amazonaws.com/isupportcause/uploads/overlay/isupportimg_1511771056271.png",
        token: nanoid(),
    });

    await TrackHistory.create({
        track: "Jocelyn Flores",
        time: "2020-11-19T10:05:25.835Z",
        user: user.username,
        artist: XXXTENTACION.name,
    });

    db.close();
});





const router = require("express").Router();
const multer = require("multer");
const path = require("path");
const {nanoid} = require("nanoid");
const config = require("../config");
const Artist = require("../models/Artist");
const auth = require("../middleware/auth");
const permit = require("../middleware/permit");

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, config.uploadPath);
  },
  filename: (req, file, cb) => {
    cb(null, nanoid() + path.extname(file.originalname));
  }
});

const upload = multer({storage});

router.get("/", async (req, res) => {
  try {
    const artists = await Artist.find({"published": true});
    res.send(artists);
  } catch (e) {
    res.status(500).send(e);
  }
});

router.get("/unpublished", [auth, permit('admin')], async (req,res) => {
  try {
    const unpublishedArtists = await Artist.find({"published": false});
    res.send(unpublishedArtists);
  } catch (e) {
    res.status(400).send(e);
  }
});

router.delete("/:id", [auth, permit('admin')], async (req, res) => {
  const artist = await Artist.findById(req.params.id).populate("artist");
  if (!artist) {
    return res.status(404).send("Artist doesn't found");
  }
  try {
    await Artist.findByIdAndDelete(req.params.id);
    res.send("Deleted")
  } catch (e) {
    res.status(400).send(e);
  }
});

router.post("/published/:id", [auth, permit('admin')], async (req, res) => {
  const artist = await Artist.findById(req.params.id).populate("artist");
  if (!artist) {
    return res.status(404).send("Artist doesn't found");
  }
  try {
    artist.published = true;
    await artist.save();
  } catch (e) {
    res.status(400).send(e);
  }
});

router.post("/", upload.single("image"), auth, async (req, res) => {
  const artistData = req.body;
  if (req.file) {
    artistData.image = req.file.filename;
  }
  const artist = new Artist(artistData);
  try {
    await artist.save();
    res.send(artist);
  } catch(e) {
    res.status(400).send(e);
  }
});

module.exports = router;
const router = require("express").Router();
const TrackHistory = require("../models/TrackHistory");
const Track = require("../models/Track");
const User = require("../models/User");
const auth = require("../middleware/auth");

router.get("/:user", async (req, res) => {
    try {
        const trackHistory = await TrackHistory.find({'user': req.params.user}).sort({"time": -1});
        res.send(trackHistory);
    } catch (e) {
        res.sendStatus(500);
    }
});

router.post("/", auth, async (req, res) => {
    const track = await Track.findById(req.body.track);
    if (!track) {
        return res.status(401).send({error: "Track does not exist"});
    }

    try {
        const trackHistory = new TrackHistory(req.body);
        trackHistory.generateTime();
        trackHistory.generateUser(req.author.username);
        trackHistory.generateArtist(req.body.artist);
        trackHistory.generateTrack(track.name);
        await trackHistory.save();
        res.send(trackHistory);
    } catch (e) {
        return res.status(401).send("Unauthorized");
    }
});

module.exports = router;
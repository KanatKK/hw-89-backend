const router = require("express").Router();
const Album = require("../models/Album");
const Track = require("../models/Track");
const auth = require("../middleware/auth");
const permit = require("../middleware/permit");

router.get("/", async (req, res) => {
    try {
        const tracks = await Track.find({"album": req.query.album, "published": true}).sort({"number": 1}).populate("album");
        res.send(tracks);
    } catch (e) {
        res.status(500).send(e);
    }
});

router.post("/published/:id", [auth, permit('admin')], async (req, res) => {
    const track = await Track.findById(req.params.id).populate("album");
    if (!track) {
        return res.status(404).send("Track doesn't found");
    }
    try {
        track.published = true;
        await track.save();
    } catch (e) {
        res.status(400).send(e);
    }
});

router.delete("/:id", [auth, permit('admin')], async (req, res) => {
    const track = await Track.findById(req.params.id)
    if (!track) {
        return res.status(404).send("Track doesn't found");
    }
    try {
        await Track.findByIdAndDelete(req.params.id);
        res.send("Deleted")
    } catch (e) {
        res.status(400).send(e);
    }
});

router.get("/unpublished", [auth, permit('admin')], async (req,res) => {
    try {
        const unpublishedTracks = await Track.find({"published": false}).sort({"number": 1}).populate("artist");
        res.send(unpublishedTracks);
    } catch (e) {
        res.status(400).send(e);
    }
});

router.post("/", auth, async (req, res) => {
    const trackData = req.body;
    const album = await Album.findById(req.body.album);
    if (!album) return res.status(400).send("Album does not exist");
    const track = new Track(trackData);
    try {
        await track.save();
        res.send(track);
    } catch(e) {
        res.status(400).send(e);
    }
});

module.exports = router;
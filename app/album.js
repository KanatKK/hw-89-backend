const router = require("express").Router();
const multer = require("multer");
const path = require("path");
const {nanoid} = require("nanoid");
const config = require("../config");
const Album = require("../models/Album");
const Artists = require("../models/Artist");
const auth = require("../middleware/auth");
const permit = require("../middleware/permit");

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const upload = multer({storage});

router.get("/", async (req, res) => {
    if (req.query.artist) {
        try {
            const albums = await Album.find({"published": true, "artist": req.query.artist}).sort({"year": 1}).populate("artist");
            res.send(albums);
        } catch (e) {
            res.status(500).send(e);
        }
    } else {
        try {
            const albums = await Album.find({"published": true}).sort({"year": 1}).populate("artist");
            res.send(albums);
        } catch (e) {
            console.log(e);
        }
    }
});

router.post("/published/:id", [auth, permit('admin')], async (req, res) => {
    const album = await Album.findById(req.params.id).populate("artist");
    if (!album) {
        return res.status(404).send("Album doesn't found");
    }
    try {
        album.published = true;
        await album.save();
    } catch (e) {
        res.status(400).send(e);
    }
});

router.delete("/:id", [auth, permit('admin')], async (req, res) => {
    const album = await Album.findById(req.params.id)
    if (!album) {
        return res.status(404).send("Album doesn't found");
    }
    try {
        await Album.findByIdAndDelete(req.params.id);
        res.send("Deleted")
    } catch (e) {
        res.status(400).send(e);
    }
});

router.get("/unpublished", [auth, permit('admin')], async (req,res) => {
    try {
        const unpublishedAlbum = await Album.find({"published": false});
        res.send(unpublishedAlbum);
    } catch (e) {
        res.status(400).send(e);
    }
})

router.post("/", upload.single("image"), auth, async (req, res) => {
    const albumData = req.body;
    if (req.file) {
        albumData.image = req.file.filename
    }
    const artist = await Artists.findById(req.body.artist);
    if (!artist) return res.status(400).send("Artist does not exist");
    const album = new Album(albumData);
    try {
        await album.save();
        res.send(album);
    } catch(e) {
        res.status(400).send(e);
    }
});

module.exports = router;
const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const albumSchema = new Schema({
    name: {
        type: String,
        required: true
    },
    artist: {
        type: Schema.Types.ObjectId,
        ref: "Artist",
        required: true
    },
    year: {
        type: Number,
        required: true
    },
    image: String,
    published: {
        type: Boolean
    }
});

const Album = mongoose.model("Album", albumSchema);
module.exports = Album;
const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const TrackHistorySchema = new Schema({
    track: {
        type: String,
        required: true
    },
    time: {
        type: String,
        required: true
    },
    artist: {
        type: String,
        required: true
    },
    user: {
        type: String,
        required: true
    }
});

TrackHistorySchema.methods.generateTime = function () {
    const date = new Date();
    this.time = date.toISOString();
};

TrackHistorySchema.methods.generateArtist = function (artist) {
    this.artist = artist;
};

TrackHistorySchema.methods.generateUser = function (user) {
    this.user = user;
};

TrackHistorySchema.methods.generateTrack = function (track) {
    this.track = track;
};

const TrackHistory = mongoose.model("TrackHistory", TrackHistorySchema);
module.exports = TrackHistory;
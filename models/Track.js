const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const trackSchema = new Schema({
    name: {
        type: String,
        required: true
    },
    album: {
        type: Schema.Types.ObjectId,
        ref: "Album",
        required: true
    },
    duration: {
        type: String,
        required: true,
    },
    number: {
        type: Number,
        required: true
    },
    published: {
        type: Boolean
    }
});

const Track = mongoose.model("Track", trackSchema);
module.exports = Track;
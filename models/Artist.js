const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const ArtistSchema = new Schema({
  name: {
    type: String,
    required: true
  },
  description: {
    type: String,
    required: true
  },
  image: String,
  published: {
    type: Boolean
  }
});

const Artist = mongoose.model("Artist", ArtistSchema);
module.exports = Artist;